# TextSecure Backup Viewer
View TextSecure (now Signal) plaintext backups in a pleasing way.

It works 100% offline by reading the XML file.

You can also supply a .vcf file exported from your stock Android phone book.
It then uses names and photos from there.

## Demo

![Screenshot](textsecure_backup_viewer_demo.png)
